/**
 * Bài 1
 */
var num1, num2, num3;
var pos1, pos2, pos3;

function sapXep() {
  var min = false;
  var max = false;
  num1 = document.getElementById("num1").value;
  num2 = document.getElementById("num2").value;
  num3 = document.getElementById("num3").value;

  if (num1 == "" || num2 == "" || num3 == "") {
    //có số rỗng
    alert("Vui lòng nhập dữ liệu");
  } else {
    //3 số nhập hợp lệ
    num1 = document.getElementById("num1").value * 1;
    num2 = document.getElementById("num2").value * 1;
    num3 = document.getElementById("num3").value * 1;
    if (num1 <= num2) {
      if (num1 <= num3) {
        // num1 là số min, xuất num1 tại pos1, cho min = true
        document.getElementById("pos1").innerText = num1 + ", ";
        min = true;
      } else {
        // num3 < num1 < num2, xuất ra màn hình từng vị trí & kết thúc
        document.getElementById("pos1").innerText = num3 + ", ";
        document.getElementById("pos2").innerText = num1 + ", ";
        document.getElementById("pos3").innerText = num2;
      }
    } else {
      if (num1 >= num3) {
        // num1 là số max
        document.getElementById("pos3").innerText = num1;
        max = true;
      }
    }
    if (min) {
      // so sánh num2 và num3 cho vị trí pos2 và pos3
      if (num2 <= num3) {
        document.getElementById("pos2").innerText = num2 + ", ";
        document.getElementById("pos3").innerText = num3;
      } else {
        document.getElementById("pos2").innerText = num3 + ", ";
        document.getElementById("pos3").innerText = num2;
      }
    }
    if (max) {
      // so sánh num2 và num3 cho ví trí pos1 và pos2
      if (num2 <= num3) {
        document.getElementById("pos1").innerText = num2 + ", ";
        document.getElementById("pos2").innerText = num3 + ", ";
      } else {
        document.getElementById("pos1").innerText = num3 + ", ";
        document.getElementById("pos2").innerText = num2 + ", ";
      }
    }
  }
}

/**
 * Bài 2
 */

function chaoHoi() {
  // lấy giá trị 5 số
  var thanhVien = document.getElementById("thanhVien").value;
  if (thanhVien == "B") {
    document.getElementById("xinChao").innerHTML = "Xin chào Bố";
  } else if (thanhVien == "M") {
    document.getElementById("xinChao").innerHTML = "Xin chào Mẹ";
  } else if (thanhVien == "A") {
    document.getElementById("xinChao").innerHTML = "Xin chào Anh Trai";
  } else if (thanhVien == "E") {
    document.getElementById("xinChao").innerHTML = "Xin chào Em Gái";
  } else {
    document.getElementById("xinChao").innerHTML = "Xin chào Người lạ ơi!";
  }
  // xuất ra màn hình
}
/**
 * Bài 3
 */

function demChanle() {
  var so1B3 = document.getElementById("so1B3").value;
  var so2B3 = document.getElementById("so2B3").value;
  var so3B3 = document.getElementById("so3B3").value;
  var soChan = 0;
  if (so1B3 == "" || so2B3 == "" || so3B3 == "") {
    alert("Vui lòng nhập dữ liệu");
  } else {
    so1B3 = so1B3 * 1;
    so2B3 = so2B3 * 1;
    so3B3 = so3B3 * 1;

    if (so1B3 % 2 == 0) {
      soChan += 1;
    }
    if (so2B3 % 2 == 0) {
      soChan += 1;
    }
    if (so3B3 % 2 == 0) {
      soChan += 1;
    }
    // xuất kết quả
    document.getElementById("demSo").innerHTML = `Có ${soChan} số chẵn và ${
      3 - soChan
    } số lẻ`;
  }
}

/**
 * Bài 4
 */
var a; //cạnh 1
var b; //cạnh 2
var c; //cạnh 3

function loaiTamgiac() {
  a = document.getElementById("canh1").value;
  b = document.getElementById("canh2").value;
  c = document.getElementById("canh3").value;
  var isTamgiac = false;
  if (a == "" || b == "" || c == "" || a <= 0 || b <= 0 || c <= 0) {
    alert("Dữ liệu không hợp lệ!");
  } else {
    a = a * 1;
    b = b * 1;
    c = c * 1;
    if (a < b + c && b < a + c && c < a + b) {
      isTamgiac = true;
    }
    if (isTamgiac) {
      if (a == b || a == c || b == c) {
        if (a == b && a == c) {
          document.getElementById("tamGiac").innerText = "Đây là tam giác đều";
        } else {
          document.getElementById("tamGiac").innerText = "Đây là tam giác cân";
        }
      } else {
        if (
          a * a == b * b + c * c ||
          b * b == a * a + c * c ||
          c * c == a * a + b * b
        ) {
          document.getElementById("tamGiac").innerText =
            "Đây là tam giác vuông";
        } else {
          document.getElementById("tamGiac").innerText =
            "Đây là tam giác bình thường";
        }
      }
    } else {
      document.getElementById("tamGiac").innerText =
        "3 độ dài này không tạo thành 1 tam giác!";
    }
  }
}
